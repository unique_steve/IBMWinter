package sk.ibm.sa;

import java.io.Serializable;

public class Address implements Serializable{

	private int id_address;
	/**
	 * Street name including street number. 
	 */
	private String street;
	
	private String city;
	
	/**
	 * Called also zipCode.
	 */
	private String postalCode;

	private Country country;
		
	public Address() {
		
	}
	
	public Address(String street, String city, String postalCode, Country country) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public Country getCountry() {
		return country;
	}
	
	@Override
	public String toString() {
		return street + "/" + city + "/" + country;
	}

	public int getId_address() {
		return id_address;
	}

	public void setId_address(int id_address) {
		this.id_address = id_address;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	
}
