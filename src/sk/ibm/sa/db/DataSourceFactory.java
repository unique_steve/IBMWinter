package sk.ibm.sa.db;

import org.hsqldb.jdbc.JDBCDataSource;

public class DataSourceFactory {

	private static final DataSourceFactory INSTANCE = new DataSourceFactory();
	private static final String URL = "jdbc:hsqldb:hsql://localhost/ebayWebDB";
	private static final String USERNAME = "SA";
	private static final String PASSWORD = "";
	
	private JDBCDataSource dataSource;
	
	private DataSourceFactory() {
	}
		
	public static DataSourceFactory getInstance() {
		return INSTANCE;
	}
	
	public JDBCDataSource getDataSource() {
		if (dataSource == null) {
			dataSource = new JDBCDataSource();
			dataSource.setURL(URL);
			dataSource.setUser(USERNAME);
			dataSource.setPassword(PASSWORD);
		}
		return dataSource;
	}
	
	public JDBCDataSource getDataSourceForTesting() {
		// ak by sme mali specialnu databazu na testovanie tak tu pridat vytvorenie datasource
		return getDataSource();
	}
	
	
}
