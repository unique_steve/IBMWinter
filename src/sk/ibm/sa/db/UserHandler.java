package sk.ibm.sa.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.AbstractListHandler;

import sk.ibm.sa.Address;
import sk.ibm.sa.Country;
import sk.ibm.sa.User;
import sk.ibm.sa.exceptions.EmailFormatException;

public class UserHandler extends AbstractListHandler<User> {

	@Override
	protected User handleRow(ResultSet rs) throws SQLException {
		try {
			Address a = new Address(rs.getString("street"), rs.getString("city"), rs.getString("postalCode"),
					Country.valueOf(rs.getString("country").toUpperCase()));
			a.setId_address(rs.getInt("id_address"));
			User user = new User(rs.getString("name"), rs.getString("surname"), 
					a, rs.getString("email"),
					rs.getString("password"));
			user.setId_user(rs.getInt("id_user"));
			return user;
		} catch (EmailFormatException e) {
			e.printStackTrace();
		}
		return null;

	}

}
