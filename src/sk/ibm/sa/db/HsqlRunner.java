package sk.ibm.sa.db;

import org.hsqldb.server.Server;

public class HsqlRunner {

	public static void main(String[] args) {
		Server server = new Server();
		server.setDatabasePath(0, "db/ebayWebDB");
		server.setDatabaseName(0, "ebayWebDB");
		server.start();
	}
	
}
