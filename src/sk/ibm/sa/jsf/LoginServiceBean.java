package sk.ibm.sa.jsf;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import sk.ibm.sa.Country;
import sk.ibm.sa.dao.UserController;
import sk.ibm.sa.utils.LoginUtils;
import sk.ibm.sa.utils.SessionUtils;

@Named("login")
@SessionScoped
public class LoginServiceBean implements Serializable{

	private UserController userController;
	
	private String email;
	/**
	 * password - unhashed
	 */
	private String password;
	
	private boolean isUserLogged;
	
	public LoginServiceBean() {
		userController = new UserController();
		isUserLogged = false;
	}
	
	public Country[] getCountries() {
		return Country.values();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isUserLogged() {
		return isUserLogged;
	}

	public String login() {

		boolean valid = userController.hasUser(email, password);
		if (valid) {
			HttpSession httpSession = SessionUtils.getSession();
			httpSession.setAttribute("email", email);
			return "dashboard";
		} else {
			return "index";
		}
	}
	
	public String logout(){
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "index";
	}
}
