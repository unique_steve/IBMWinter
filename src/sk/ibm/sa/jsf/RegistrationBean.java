package sk.ibm.sa.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import sk.ibm.sa.User;
import sk.ibm.sa.dao.UserController;

@Named("reg")
@RequestScoped
public class RegistrationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserController userController = new UserController();
	
	private User user;
	
	public RegistrationBean() {
	
	}
	
	@PostConstruct
	public void init() {
		user = new User();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void register() {
		userController.addUser(user);
	}
	
	

}
