package sk.ibm.sa.jsf;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "loginFilter", urlPatterns = { "*.xhtml" })
public class LoginFilter implements Filter {

	public LoginFilter(){
		
	}
	
	@Inject
	private LoginServiceBean loginBean;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		try{
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			HttpSession httpSession = httpServletRequest.getSession(false);
			
			String httpServletRequestURI = httpServletRequest.getRequestURI();
			if(httpServletRequestURI.indexOf("/index.xhtml") >= 0
					|| (httpSession != null && httpSession.getAttribute("email") != null)
					|| httpServletRequestURI.indexOf("/public/") >= 0
					|| httpServletRequestURI.contains("javax.faces.resource"))
				chain.doFilter(request, response);
			else
				 httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/index.xhtml");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}

	@Override
	public void destroy() {
	}

}
