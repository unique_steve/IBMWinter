package sk.ibm.sa;

import java.io.Serializable;

import sk.ibm.sa.exceptions.EmailFormatException;
import sk.ibm.sa.utils.LoginUtils;

public class User implements Comparable<User>, Serializable {

	private int id_user;

	private String name;
	private String surname;

	private Address address;
	private String email;
	private String password;

	public User() {
		address = new Address();
	}

	public User(String name, String surname, Address address, String email, String password)
			throws EmailFormatException {
		this.name = name;
		this.surname = surname;
		this.address = address;
		setEmail(email);
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public void setEmail(String email) throws EmailFormatException {
		if (!LoginUtils.isEmailValid(email)) {
			throw new EmailFormatException("Invalid format of email: " + email);
		}
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = LoginUtils.hash(password);
	}

	@Override
	public String toString() {
		return name + " " + surname + " (" + address + "), " + email;
	}

	@Override
	public int compareTo(User o) {
		if (this.surname.equals(o.getSurname())) {
			return this.name.compareTo(o.getName());
		} else {
			return this.surname.compareTo(o.getSurname());
		}

	}

}
