package sk.ibm.sa.dao;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import sk.ibm.sa.Country;
import sk.ibm.sa.User;
import sk.ibm.sa.utils.LoginUtils;

public class UserController {
	
	private UserDao userDao;
	
	public UserController() {
		userDao = UserDaoFactory.getInstance().getUserDao();
	}

	public void addUser(User user) {
		userDao.addUser(user);	
	}

	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}
	
	public List<User> getAllUsersSortedByCountry() {
		 List<User> allUsers = userDao.getAllUsers();
		 Collections.sort(allUsers, new Comparator<User>() {
			@Override
			public int compare(User o1, User o2) {
				return o1.getAddress().getCountry().toString().compareTo(o2.getAddress().getCountry().toString());
			}
		});
		 return allUsers;
	}
	
	public List<User> getAllUsersSortedByName() {
		 List<User> allUsers = userDao.getAllUsers();
		 Collections.sort(allUsers);
		 return allUsers;
	}
	
	public List<User> getAllUsersSortedByEmailLength() {
		 List<User> allUsers = userDao.getAllUsers();
		 Collections.sort(allUsers, (User u1, User u2) -> u1.getEmail().length() - u2.getEmail().length());
		 return allUsers;
	}
	
	public List<User> getSlovakUsers() {
		 return userDao.getAllUsers().stream().filter(p -> p.getAddress().getCountry() 
				 == Country.SLOVAKIA).collect(Collectors.toList());
	}
	
	public double getAverageEmailLength() {
		return userDao.getAllUsers().stream().mapToInt(p -> p.getEmail().length()).average().getAsDouble();
	}
	
	public long getDistinctNameCount() {
		// musime pouzit map aby sme pracovali s menami nie pouzivatelmi
		return userDao.getAllUsers().stream().map(p -> p.getName()).distinct().count();
	}
	
	public long getUserCount() {
		return userDao.getUserCount();
	}
	
	/**
	 * 
	 * @param email
	 * @param password - unhashed
	 * @return
	 */
	public boolean hasUser(String email, String password) {
		String passHash = LoginUtils.hash(password);
		return userDao.hasUser(email, passHash);
	}
	
	
	
}
