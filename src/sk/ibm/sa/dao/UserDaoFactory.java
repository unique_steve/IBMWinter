package sk.ibm.sa.dao;

public class UserDaoFactory {

	private static final UserDaoFactory INSTANCE = new UserDaoFactory();
	
	private UserDao userDao;
	
	public static UserDaoFactory getInstance() {
		return INSTANCE;
	}
	
	private UserDaoFactory() {
		
	}
	
	public UserDao getUserDao() {
		if (userDao == null) {
			userDao = new DbUserDao();
		}
		return userDao;
	}
	
}
