package sk.ibm.sa.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryLoader;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import sk.ibm.sa.Address;
import sk.ibm.sa.User;
import sk.ibm.sa.db.DataSourceFactory;
import sk.ibm.sa.db.UserHandler;
import sk.ibm.sa.exceptions.DatabaseException;

public class DbUserDao implements UserDao {

	private QueryRunner queryRunner;
	private QueryLoader queryLoader;
	private static final String SQL_FILE = "/sql.xml";

	public DbUserDao() {
		DataSource dataSource = DataSourceFactory.getInstance().getDataSource();
		queryRunner = new QueryRunner(dataSource);
		queryLoader = QueryLoader.instance();
	}
	
	public void useTestingDatabase() {
		queryRunner = new QueryRunner(DataSourceFactory.getInstance().getDataSourceForTesting());
	}

	public QueryRunner getQueryRunner() {
		return queryRunner;
	}
	
	@Override
	public void addUser(User user) {
		try {
			Address a = user.getAddress();
			String sqlSelectAddress = getQuery("selectAddress");
			Integer id_address = queryRunner.query(sqlSelectAddress, new ScalarHandler<Integer>(), a.getStreet(),
					a.getCity(), a.getPostalCode(), a.getCountry().toString());
			if (id_address == null) {
				id_address = queryRunner.insert(getQuery("insertAddress"), new ScalarHandler<Integer>(), a.getStreet(),
						a.getCity(), a.getPostalCode(), a.getCountry().toString());
			}
			a.setId_address(id_address);
			// v tabulke sa nachadza dana adresa a jej id je ulozene v id_address
			Integer id_user = queryRunner.insert(getQuery("insertUser"), new ScalarHandler<Integer>(), 
					user.getName(), user.getSurname(), id_address, user.getEmail(), user.getPassword());
			user.setId_user(id_user);
		} catch (SQLException e) {
			throw new DatabaseException("Add User failed", e);
		}
	}
	
	public String getQuery(String query) {
		try {
			Map<String, String> queries = queryLoader.load(SQL_FILE);
			return queries.get(query);
		} catch (IOException e) {
			throw new DatabaseException("SQL query loading failed", e);
		}
	}

	@Override
	public List<User> getAllUsers() {
		String sql = getQuery("selectUsers");
		try {
			return queryRunner.query(sql, new UserHandler());
		} catch (SQLException e) {
			throw new DatabaseException("Select from User failed", e);
		}
	}

	@Override
	public long getUserCount() {
		String sql = getQuery("userCount");
		try {
			return queryRunner.query(sql, new ScalarHandler<Long>());
		} catch (SQLException e) {
			throw new DatabaseException("Select count failed", e);
		}
	}
	
	@Override
	public boolean hasUser(String email, String password) {
		String sql = getQuery("selectLoginUser");
		try {
			return queryRunner.query(sql, new ScalarHandler<Long>(), email, password) == 1;
		} catch (SQLException e) {
			throw new DatabaseException("Select login failed", e);
		}
	}


	@Override
	public void deleteAllUsers() {
		// TODO Auto-generated method stub

	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
