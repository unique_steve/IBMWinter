package sk.ibm.sa.dao;
import java.util.List;

import sk.ibm.sa.User;

public interface UserDao {

	public void addUser(User user);
	
	public List<User> getAllUsers();
	
	public long getUserCount();

	public void deleteAllUsers();
	
	public User getUserByEmail(String email); 
	
	/**
	 * interface
	 * @param email
	 * @param password - hash
	 * @return
	 */
	public boolean hasUser(String email, String password);  
	
}
